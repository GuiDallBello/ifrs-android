/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.datafrominternet;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    // TODO (26) Crie uma variável do tipo EditText chamada mSearchBoxEditText

    // TODO (27) Crie uma variátel do tipo TextView chamada mUrlDisplayTextView
    // TODO (28) Crie uma variátel do tipo TextView chamada mSearchResultsTextView

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TODO (29) Use findViewById para obter a referência para mSearchBoxEditText

        // TODO (30) Use findViewById para obter a referência para mUrlDisplayTextView
        // TODO (31) Use findViewById para obter a referência para mSearchResultsTextView
    }
}
